from selenium import webdriver


def minusculas_sin_signos(texto):
    # Elimina tildes y mayusculas de un texto
    texto = texto.lower()
    texto = texto.replace('á', 'a')
    texto = texto.replace('é', 'e')
    texto = texto.replace('í', 'i')
    texto = texto.replace('ó', 'o')
    texto = texto.replace('ú', 'u')

    return texto


def obtener_cuerpo(url):
    # Dado el link de una noticia retorna su texto
    path = 'D:/phantomjs/bin/phantomjs.exe'
    driver = webdriver.PhantomJS(executable_path=path)
    driver.get(url)
    texto_bruto = texto = ''

    elementos = driver.find_elements_by_id('article-content')
    for a in elementos:
        texto_bruto += str(a.text)

    texto_bruto = minusculas_sin_signos(texto_bruto)

    texto_lista = texto_bruto.split('\n')

    for a in texto_lista:
        if 'mas sobre este tema' in a:
            break
        texto += a
        texto += '\n'

    return texto

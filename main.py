__author__ = 'Ignacio Pieve'
__version__ = 2.0

import os
import time
from selenium import webdriver
import get_body_text
import database
import extras
import warnings
import sqlite3


def interfaz():
    # Retornar url, xpath y palabras del usuario
    while True:
        opciones = ['ultimas-noticias/', 'tag/yemen/', 'tag/siria/', 'tag/venezuela/']
        xpaths = ['//*[@id="fpxzHf2aS0RE3r"]/div[2]', '//*[@id="fWV3HS1oDn8f2r"]/div[2]', '//*[@id="fWV3HS1oDn8f2r"]/div[2]', '//*[@id="fWV3HS1oDn8f2r"]/div[2]']
        id_tags = ['fpxzHf2aS0RE3r', 'fWV3HS1oDn8f2r', 'fWV3HS1oDn8f2r', 'fWV3HS1oDn8f2r']

        print('poner aca un mensaje de bienvenida\n')
        print('Links disponibles:\n'
              '0- Ultimas noticias\n'
              '1- Tag: Yemen\n'
              '2- Tag: Siria\n'
              '3- Tag: Venezuela\n')
        opcion = int(input('Elija una opcion: '))

        url = 'https://www.infobae.com/' + opciones[opcion]
        xpath = xpaths[opcion]
        tag = (opciones[opcion].replace('tag/', '')).replace('/', '')
        id_tag = id_tags[opcion]

        print('\nA continuacion ingrese las palabras que desea buscar (digite 0 para terminar): ')
        lista = []
        c = 0
        while True:
            c += 1
            palabra = input('Palabra ' + str(c) + ': ')
            if palabra == '0':
                break
            lista.append(palabra)

        print('\nTodo correcto, los datos son: ')
        print('URL:', url)
        for a in range(len(lista)):
            print('Palabra ', a, ': ', lista[a], sep='')

        seleccion = input('\nLos datos son correctos? (y/n): ')
        if seleccion == 'n':
            print('REINICIANDO...')
            time.sleep(2)
            os.system('cls')
        else:
            break
    os.system('cls')

    return url, xpath, lista, tag, id_tag


def obtener_links_anteriores(repeticiones, url, boton_xpath, id_tag):
    # Aprieta el boton bajar 'repeticiones' veces, obtiene todos los links y los guarda en
    path = 'D:\phantomjs/bin\phantomjs.exe'
    driver = webdriver.PhantomJS(executable_path=path)
    driver.get(url)
    cuerpo_links = driver.find_element_by_id(id_tag)
    link_actual = ''
    links = []

    if url == 'https://www.infobae.com/ultimas-noticias/':
        multiplicar = 40
    else:
        multiplicar = 10

    for a in range(repeticiones):
        cuerpo_links.find_element_by_xpath(boton_xpath).click()
        print("Obteniendo links de pagina " + str(a+1))
        print("Noticias escaneadas:", (int(a)+1)*multiplicar)
        time.sleep(1)

    print('\nREGISTRANDO...')

    elementos = cuerpo_links.find_elements_by_class_name('col-sm-8')
    n = len(elementos)
    for a in range(n):
        x = elementos[a].find_element_by_tag_name('a')
        link_actual = str(x.get_attribute('href')).replace('https://www.infobae.com', '')
        links.append(link_actual)

    print('LINKS REGISTRADOS\n')

    a = link_actual.find('201')
    fecha = ''
    for i in range(a, a + 10):
        fecha += link_actual[i]

    print('La ultima fecha registrada es:', fecha)
    print('Cantidad de links encontrados:', n)

    return links


def buscador_palabras(tag, lista_links, lista_palabras):
    cant_palabras = len(lista_palabras)
    repeticiones_palabras = [0] * cant_palabras
    repeticiones_palabras_total = [0] * cant_palabras

    conexion = sqlite3.connect('b_datos/base_datos.db')
    consulta = conexion.cursor()

    palabras_columnas = """"""
    for b in range(cant_palabras):
        palabras_columnas += ', ' + lista_palabras[b]

    cont = 0
    for a in lista_links:
        cont += 1
        print('Link ', cont, ': ', a, sep='')
        texto = get_body_text.obtener_cuerpo(str('https://www.infobae.com' + a))
        for i in range(cant_palabras):
            repeticiones_palabras[i] = texto.count(lista_palabras[i])
            repeticiones_palabras_total[i] += repeticiones_palabras[i]

        tiene = 0
        for i in repeticiones_palabras:
            if i > 0:
                tiene = 1
                break

        sql = """
        INSERT INTO '""" + tag + """_links'(URL""" + palabras_columnas + """, TIENE)
        VALUES(""" + '?, ' * cant_palabras + """?, ?)
        """

        valores = [a] + repeticiones_palabras + [tiene]

        consulta.execute(sql, valores)

    palabras_columnas_general = list(palabras_columnas)
    palabras_columnas_general[0] = ''
    palabras_columnas_general[1] = ''
    palabras_columnas_general = ''.join(palabras_columnas_general)

    tag_general = """
        INSERT INTO '""" + tag + """_general'(""" + palabras_columnas_general + """)
        VALUES(?""" + ', ?' * (cant_palabras - 1) + """)
        """

    consulta.execute(tag_general, repeticiones_palabras_total)

    # guardamos y cerramos
    conexion.commit()
    conexion.close()


def main():
    url, xpath, palabras, tag, id_tag = interfaz()
    repeticiones = int(input('Cuantas veces desea bajar?: '))
    print('COMENZANDO ANALISIS')


    extras.crear_carpetas()
    database.crear_b_datos_general()
    database.crear_b_datos_tag(tag, palabras)

    l_links = obtener_links_anteriores(repeticiones, url, xpath, id_tag)
    for a in range(len(l_links)):
        print(a, '- ', l_links[a], sep='')

    input('Presione enter para contrinuar')
    os.system('cls')

    print('Comenzado a buscar las palabras indicadas.\n\n')

    buscador_palabras(tag, l_links, palabras)

    database.add_datos_general(tag, palabras, len(l_links))

    print('\nFINALIZADO.\n')

    input('Presione enter para salir')


if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    main()
